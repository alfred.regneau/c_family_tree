# compilateur
CC := gcc
# options de compilation
CFLAGS := -std=c99 -Wall -Wextra -pedantic -ggdb

# règle de compilation --- exécutables
all : executable

executable : main.o  familytree.o mcduck.o
	$(CC) $(CFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

# options de compilation
memoire : executable
	valgrind --leak-check=full ./executable

clean:
	rm *.o executable
