#include "familytree.h"


struct family_tree {
    family_member* roots;
    int nb;
};

struct family_member {
    //personnage de type générique
    void* personage;
    //mari/femme
    family_member spouse;
    //frere/soeur
    family_member sibling;
    //enfant
    family_member child;
    //parent1
    family_member parent1;
    //parent2
    family_member parent2;
    //fonction d'affichage du personnage
    void(*_print)(void *);
};


/*! Fonction de création d'un arbre généalogique
 * \return l'arbre créé
 */
family_tree init_tree(){
    family_tree ft = malloc(sizeof(struct family_tree));
    ft->roots = NULL;
    ft->nb = 0;
    return ft;
}

/*! Fonction d'ajout d'un noeud racine dans l'arbre
 * \param ft l'arbre auquel ajouté la racine
 * \param fm le noeud racine à ajouter
 */
void add_root(family_tree ft, family_member fm) {
    ft->nb++;
    ft->roots = realloc(ft->roots, ft->nb * sizeof( struct family_member));
    ft->roots[(ft->nb)-1] = fm;
}

/*! Fonction de destruction d'un arbre généalogique
 * \param ft un pointeur sur un arbre généalogique
 * \param is_same une fonction déterminant l'égalité de deux personnages
 */
void delete_tree(family_tree *ft, bool(*is_same)(void*, void*)){
    family_tree tree = *ft;
    for (int i = 0; i < tree->nb; i++)
        delete_from_node(&tree->roots[i], is_same);
    free(tree->roots);
    free(tree);
    tree = NULL;
}


/*! Fonction de recherche d'un personnage dans un arbre généalogique
 * \param ft l'arbre généalogique
 * \param is_same une fonction déterminant l'égalité de deux personnages
 */
family_member search_in_tree(family_tree ft, void* pers, bool(*is_same)(void*, void*)) {
    for (int i = 0; i < ft->nb; i++) {
        family_member fm = search(ft->roots[i], pers, is_same);
        if (fm != NULL)
            return fm;
    }
    return NULL;
}


/*! Fonction de création d'un noeud de type family_member
 * \param personage un personnage de type générique
 * \param print une fonction d'affichage de ce personnage
 * \return le noeud créé
 */
family_member create_node(void* personage, void(*print)(void*)) {
    family_member fm = (family_member) malloc(sizeof(struct family_member));
    fm->personage = personage;
    fm->spouse = NULL;
    fm->sibling = NULL;
    fm->child = NULL;
    fm->parent1 = NULL;
    fm->parent2 = NULL;
    fm->_print = print;
    return fm;
}


/*! Fonction ajoutant un frère ou une soeur à un noeud de l'arbre (en le plaçant le plus à droite possible parmi tous les siblings)
 * \param fm1 le noeud de départ
 * \param fm2 le noeud a ajouter
 */
void add_sibling(family_member fm1, family_member fm2) {
    if (fm1->sibling == NULL)
        fm1->sibling = fm2;
    else{
        add_sibling(fm1->sibling, fm2);

    }
}


/*! Fonction ajoutant un enfant à deux noeuds de l'arbre (les parents), en l'ajoutant comme premier enfant si les parents ne le sont pas déjà (parents), et comme sibling du premier enfant sinon
 * \param parent1 le noeud du premier parent
 * \param parent2 le noeud du deuxième parent
 * \param child l'enfant à ajouter
 */
void add_child(family_member parent1, family_member parent2, family_member child) {
    bool already_add_sibling = 0;
    if (parent1 != NULL) {
        if (parent1->child == NULL)
            parent1->child = child;
        else {
            add_sibling(parent1->child, child);
            already_add_sibling = 1;
        }
        child->parent1 = parent1;
    }
    if (parent2 != NULL) {
        if (parent2->child == NULL)
            parent2->child = child;
        else if(!already_add_sibling)
            add_sibling(parent2->child, child);
        child->parent2 = parent2;
    }

}

/*! Fonction permettant de marrier deux noeuds de l'arbre
 * \param fm1 le premier noeud à marrier
 * \param fm2 le deuxième neoud à marrier
 */
void the_wedding_present(family_member fm1, family_member fm2) {
    fm1->spouse = fm2;
    fm2->spouse = fm1;
}

/*! Fonction d'affichage de la descendance d'un noeud de l'arbre
 * \param fm le noeud de départ
 */
void show(family_member fm) {
    if (fm->personage != NULL)
        fm->_print(fm->personage);
    if (fm->spouse != NULL) {
        printf("----");
        fm->_print(fm->spouse->personage);
    }
    if (fm->child != NULL) {
        printf(" ( ");
        show(fm->child);
        printf(")");
    }
    if (fm->sibling != NULL) {
        printf(" , ");
        show(fm->sibling);
    }
}


/*! Fonction de destruction d'un arbre de type family_member à partir d'un noeud
 * \param node le noeud de départ
 * \param is_same une fonction déterminant l'égalité de deux personnages
 */
void delete_from_node(family_member *node, bool(*is_same)(void*, void*)) {
    if (*node != NULL) {
        family_member fm = (family_member) *node;
        if (fm->personage != NULL) {
            //destruction des enfants
            if (fm->child != NULL) {
                //on récupère le sibling du premier enfant, puis on détruit l'enfant
                family_member next_sibling = fm->child->sibling;
                delete_from_node(&(fm->child), is_same);
                //on détruit les siblings suivant jusqu'à ce qu'il y en ai plus
                family_member deleted_node;
                while (next_sibling != NULL) {
                    deleted_node = next_sibling;
                    next_sibling = next_sibling->sibling;
                    delete_from_node(&deleted_node, is_same);
                }
            }
            //les parents pointent maintenant sur le sibling du noeud de départ qui est supprimé (NULL si pas de sibling)
            if (fm->parent1 != NULL)
                fm->parent1->child = fm->sibling;
            if (fm->parent2 != NULL)
                fm->parent2->child = fm->sibling;
            //"divorce" entre le noeud de départ et son/sa spouse
            if (fm->spouse != NULL) {
                (fm->spouse)->child = NULL;
                (fm->spouse)->spouse = NULL;
                fm->spouse = NULL;
            }
            //destruction du noeud
            printf("Destruction de ");
            fm->_print(fm->personage);
            free(fm->personage);
            fm->personage = NULL;
            fm->_print = NULL;
            free(fm);
            fm = NULL;
            puts("");
        }
    }
}


/*! Fonction de recherche d'un noeud dans l'arbre parmi les descendants et frère/soeur d'un noeud donné
 * \param fm le noeud de départ
 * \param pers le personnage a rechercher
 * \param is_same un pointeur sur une fonction permettant de comparer les personnages de l'arbre
 * \return le premier noeud trouvé, NULL sinon
 */
family_member search(family_member fm, void* pers, bool(*is_same)(void*, void*)){
	if(fm->personage != NULL){
		if (is_same(fm->personage,pers)){
			return fm;
		}
	}
	if (fm->child != NULL){
		family_member fm1 = fm->child;
		family_member s = search(fm1,pers,is_same);
		if 	(s != NULL){
			return s;
		}
	}
	if (fm->sibling != NULL){
		family_member fm2 = fm->sibling;
		family_member s = search(fm2,pers,is_same);
		if 	(s != NULL){
			return s;
		}
	}
	return NULL;
}
