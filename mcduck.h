#ifndef MCDUCK_H
#define MCDUCK_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

struct mcduck {
    char *name;
    char *firstname;
    char *surname;
};

typedef struct mcduck* mcduck;


/*! Fonction d'affichage générique
 * \param md la structure à afficher : l'argument est void* pour permettre la généricité
 */
void print(void* md);

/*! Fonction déterminant l'égalité de deux mcduck
 * \param m le premier élément à comparer
 * \param m1 le deuxième élément à comparer
 * \return vrai si les deux personnages sont identiques, faux sinon
 */
bool is_same(void *m, void *m1);

#endif
