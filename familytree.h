#ifndef FAMILYTREE_H
#define FAMILYTREE_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct family_tree* family_tree;
typedef struct family_member* family_member;

/*! Fonction de création d'un arbre généalogique
 * \return l'arbre créé
 */
family_tree init_tree();

/*! Fonction d'ajout d'un noeud racine dans l'arbre
 * \param ft l'arbre auquel ajouté la racine
 * \param fm le noeud racine à ajouter
 */
void add_root(family_tree ft, family_member fm);

/*! Fonction de destruction d'un arbre généalogique
 * \param ft un pointeur sur un arbre généalogique
 * \param is_same une fonction déterminant l'égalité de deux personnages
 */
void delete_tree(family_tree *ft, bool(*is_same)(void*, void*));


/*! Fonction de recherche d'un personnage dans un arbre généalogique
 * \param ft l'arbre généalogique
 * \param is_same une fonction déterminant l'égalité de deux personnages
 */
family_member search_in_tree(family_tree ft, void* pers, bool(*is_same)(void*, void*));


/*! Fonction de création d'un noeud de type family_member
 * \param personage un personnage de type générique
 * \param print une fonction d'affichage de ce personnage
 * \return le noeud créé
 */
family_member create_node(void* personage, void(*print)(void*));

/*! Fonction de création d'un noeud de type family_member
 * \param personage un personnage de type générique
 * \param print une fonction d'affichage de ce personnage
 * \return le noeud créé
 */
family_member create_node(void* personage, void(*print)(void*)) ;


/*! Fonction ajoutant un frère ou une soeur à un noeud de l'arbre (en le plaçant le plus à droite possible parmi tous les siblings)
 * \param fm1 le noeud de départ
 * \param fm2 le noeud a ajouter
 */
void add_sibling(family_member fm1, family_member fm2);


/*! Fonction ajoutant un enfant à deux noeuds de l'arbre (les parents), en l'ajoutant comme premier enfant si les parents ne le sont pas déjà (parents), et comme sibling du premier enfant sinon
 * \param parent1 le noeud du premier parent
 * \param parent2 le noeud du deuxième parent
 * \param child l'enfant à ajouter
 */
void add_child(family_member parent1, family_member parent2, family_member child);


/*! Fonction permettant de marrier deux noeuds de l'arbre
 * \param fm1 le premier noeud à marrier
 * \param fm2 le deuxième neoud à marrier
 */
void the_wedding_present(family_member fm1, family_member fm2);


/*! Fonction de recherche d'un noeud dans l'arbre parmi les descendants et frère/soeur d'un noeud donné
 * \param fm le noeud de départ
 * \param pers le personnage a rechercher
 * \param is_same un pointeur sur une fonction permettant de comparer les personnages de l'arbre
 * \return le premier noeud trouvé, NULL sinon
 */
family_member search(family_member fm, void* pers, bool(*is_same)(void*, void*));


/*! Fonction d'affichage de la descendance d'un noeud de l'arbre
 * \param fm le noeud de départ
 */
void show(family_member fm);


/*! Fonction de destruction d'un arbre de type family_member à partir d'un noeud
 * \param fm le noeud de départ
 * \param is_same une fonction déterminant l'égalité de deux personnages
 */
void delete_from_node(family_member *node, bool(*is_same)(void*, void*));

#endif
