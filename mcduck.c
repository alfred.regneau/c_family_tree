#include "mcduck.h"

/*! Fonction d'affichage générique
 * \param md la structure à afficher : l'argument est void* pour permettre la généricité
 */
void print(void* md) {

    mcduck tmp = (mcduck)md;
    strcmp((*tmp).surname,"") == 0 ? printf("%s %s\t", (*tmp).firstname, (*tmp).name) :
    printf("%s \"%s\" %s\t", (*tmp).firstname, (*tmp).surname, (*tmp).name);

}


/*! Fonction déterminant l'égalité de deux mcduck
 * \param m le premier élément à comparer
 * \param m1 le deuxième élément à comparer
 * \return vrai si les deux personnages sont identiques, faux sinon
 */
bool is_same(void *m, void *m1) {

    mcduck tmp = (mcduck)m;
    mcduck tmp1 = (mcduck)m1;
    return (strcmp(tmp->name, tmp1->name) == 0 && strcmp(tmp->firstname, tmp1->firstname) == 0 && strcmp(tmp->surname, tmp1->surname) ==0);

}
