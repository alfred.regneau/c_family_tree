#ifndef MAIN_H
#define MAIN_H

/*! Fonction de création d'un personnage mcduck
 * \param name son nom
 * \param firstname son prénom
 * \param surname son surnom
 * \return la structure créé
 */
static mcduck create_mcduck(char *name, char *firstname, char *surname);

#endif
